
object DataLocality {
    val N = 1000
    def main(args: Array[String]) = {
        val result = new RowMajorMatrix(N)
        val row = new RowMajorMatrix(N)
        val row2 = new RowMajorMatrix(N)
        val col = new ColumnMajorMatrix(N)
        val col2 = new ColumnMajorMatrix(N)
        row.randFill
        col.randFill
        row2.randFill
        col2.randFill

        println(s"Row X row:\t\t${measure(row, row2, result)}")
        println(s"Column X column:\t${measure(col, col2, result)}")
        println(s"Row X column:\t\t${measure(row, col, result)}")
        println(s"Column X row:\t\t${measure(col, row, result)}")
    }

    def measure(a: Matrix, b: Matrix, c: Matrix): Double = {
        ???
    }
}

//------------------------------------------------------------------------------
object Matrix {                                                                 
    def multiply(a: Matrix, b: Matrix, c: Matrix): Unit = {                             
        for (i <- 0 until c.n)                                                  
            for(j <- 0 until b.n)                                               
                for(k <- 0 until a.n)                                           
                    c(i, j) = a(i, k) * b(k, j)                                 
    }                                                                           
}                                                                               
                                                                                
abstract class Matrix(val n: Int) {                                             
    val data = new Array[Double](n * n)                                         
                                                                                
    def randFill() = {                                                          
        val r = scala.util.Random                                               
        for(i <- 0 until n * n) data(i) = r.nextDouble                          
    }                                                                           
                                                                                
    def index(i: Int, j: Int): Int // abstract                                  
                                                                                
    def apply(i: Int, j: Int) = data(index(i, j))                               
                                                                                
    def update(i: Int, j: Int, v: Double) = {                                   
        data(index(i, j)) = v                                                   
    }                                                                           
}                                                                               
                                                                                
class RowMajorMatrix(n: Int) extends Matrix(n) {                                
    def index(i: Int, j: Int) = i * n + j                                       
}                                                                               
                                                                                
class ColumnMajorMatrix(n: Int) extends Matrix(n) {                             
    def index(i: Int, j: Int) = j * n + i                                       
} 

//------------------------------------------------------------------------------
class TimeProbe {
    var begin: Long = 0
    var end: Long = 0

    def start() = { begin = System.nanoTime }
    def stop() = { end = System.nanoTime }

    def deltaSecs() = (end - begin) / 1e9d
    def deltaMillis() = (end - begin) / 1e6d
    def deltaMicros() = (end - begin) / 1e3d
}

//------------------------------------------------------------------------------

